﻿Imports System.ComponentModel
Imports System.Net
Imports System.Net.NetworkInformation
Imports System.Text
Imports System.Threading

Public Class Home
    Private Sub StartButton_Click(sender As Object, e As EventArgs) Handles StartButton.Click

        'Modifica controlli
        LoadingPanel.Visible = True
        MainLabel.Image = My.Resources.orangedot
        MainLabel.Text = "Potrebbe volerci qualche secondo"
        SubLabel.Text = "Tentativo di connessione"

        'Nascondi pulsante per avvio analisi
        StartButton.Visible = False

        'Aggiorna thread
        Wait()

        'Avvia worker per dati invariabili
        WorkerStaticData.RunWorkerAsync()

        'Avvia
        MeasurePing()


    End Sub

    Private Sub MeasurePing()

        'Variabili persistenti
        Dim firstPingSuccedded As Boolean = False
        Dim consecutivePingFails As Integer = 0
        Dim consecutivePingSuccess As Integer = 0
        Dim chartPingMeasures As Integer = 0

        While (True)

            'Invia ping
            Dim dato As PingReply
            Try
                dato = SendPing()
            Catch
                Continue While
            End Try

            'Aggiungi dato alla lista
            DataVar.PingList.Add(dato)

            'Calcola valori instabili
            Dim failedPings As Integer = 0
            Dim calcolatedPingNumber As Integer = 0
            Dim complessivePingAddition As Long = 0

            'Se la lista contiene più di <n> valori, togli quelli in più
            If DataVar.PingList.Count = (Settings.MaxPingTry + 1) Then DataVar.PingList.RemoveAt(0)

            'Se il valore di ping di test è giusto, calcola le percentuali di uptime e aggiorna i controlli
            If DataVar.PingList.ToList.Count >= Settings.MinimumPingTry Then

                For Each ping As PingReply In DataVar.PingList.ToList

                    'Modifica controlli
                    StatsLabel.Visible = True
                    LoadingPanel.Visible = False

                    'Controlla se il ping è fallito o meno
                    If ping.Status = IPStatus.TimedOut Then
                        failedPings += 3
                        consecutivePingFails += 1
                        consecutivePingSuccess = 0
                    End If
                    If ping.RoundtripTime > DataVar.PingRange Then failedPings += 2

                    If ping.Status = IPStatus.Success Then
                        consecutivePingSuccess += 1
                        consecutivePingFails = 0
                    End If

                    If consecutivePingSuccess = 20 Then
                        calcolatedPingNumber += 30
                        consecutivePingSuccess = 0
                    End If

                    If consecutivePingFails > 35 Then
                        failedPings += 35
                    End If

                    'Aumenta contatore
                    complessivePingAddition += ping.RoundtripTime
                    calcolatedPingNumber += 1

                Next

                'Calcola PingRange
                DataVar.PingRange = 0
                DataVar.PingRange = Math.Round((complessivePingAddition / calcolatedPingNumber) * Settings.MaximumJitterMultipler)

                'Calcola percentuali
                Dim UptimePercentage As Double = Math.Round(((calcolatedPingNumber - failedPings) * 100) / calcolatedPingNumber, 1)
                StatsLabel.Text = DataVar.Statistics & "Ping: " & dato.RoundtripTime & "ms" & vbNewLine & "Stabilità: " & UptimePercentage & "%"

                'Crea grafico e aggiorna la variabile della Y del grafico con il valore di ping calcolati corrente
                If PingChart.Series("Ping").Points.Count > (Settings.MaxPingTry - Settings.MinimumPingTry) Then
                    PingChart.Series("Ping").Points.Clear()
                    chartPingMeasures = 0
                End If

                PingChart.Series("Ping").Points.AddXY(chartPingMeasures, dato.RoundtripTime)
                chartPingMeasures += 1

                'Resetta le variabili di calcolo per aggiornare i dati
                calcolatedPingNumber = 0
                failedPings = 0

                'Mostra all'utente la stabilità della rete basandosi sulla percentuali di uptime
                Select Case UptimePercentage
                    Case = 100
                        MainLabel.Image = My.Resources.greendot
                        PingChart.Series("Ping").Color = Color.SeaGreen
                        MainLabel.Text = "Connessione ideale"
                        SubLabel.Text = "La variazione del ping è ottimale"
                    Case > 95
                        MainLabel.Image = My.Resources.greendot
                        PingChart.Series("Ping").Color = Color.SeaGreen
                        MainLabel.Text = "Connessione molto stabile"
                        SubLabel.Text = "Richieste fallite minori del 5%, variazione ping accettabile"
                    Case > 80
                        MainLabel.Image = My.Resources.greendot
                        PingChart.Series("Ping").Color = Color.SeaGreen
                        MainLabel.Text = "Connessione perlopiù stabile"
                        SubLabel.Text = "La variazione corrente del ping non dovrebbe dare problemi"
                    Case > 70
                        MainLabel.Image = My.Resources.orangedot
                        PingChart.Series("Ping").Color = Color.DarkOrange
                        MainLabel.Text = "Connessione piuttosto instabile"
                        SubLabel.Text = "Molte richieste al server sono fallite/scadute"
                    Case > 60
                        MainLabel.Image = My.Resources.orangedot
                        PingChart.Series("Ping").Color = Color.DarkOrange
                        MainLabel.Text = "Connessione instabile"
                        SubLabel.Text = "La variazione di ping è molto alta"
                    Case > 40
                        MainLabel.Image = My.Resources.orangedot
                        PingChart.Series("Ping").Color = Color.DarkOrange
                        MainLabel.Text = "Connessione molto instabile"
                        SubLabel.Text = "La maggior parte delle richieste non va a buon fine"
                    Case > 10
                        MainLabel.Image = My.Resources.reddot
                        PingChart.Series("Ping").Color = Color.Red
                        MainLabel.Text = "Connessione inusufruibile"
                        SubLabel.Text = "Variazioni di ping troppo alte per la navigazione"
                    Case > 0
                        MainLabel.Image = My.Resources.reddot
                        PingChart.Series("Ping").Color = Color.Red
                        MainLabel.Text = "Internet quasi assente"
                        SubLabel.Text = "Meno del 10% delle richieste va a buon fine"
                    Case = 0
                        PingChart.Series("Ping").Color = Color.Red
                        MainLabel.Image = My.Resources.reddot
                        MainLabel.Text = "Connessione assente"
                        SubLabel.Text = "Nessuna ultima richiesta è andata a buon fine"
                End Select
                Wait()

            ElseIf firstPingSuccedded = True Then

                'Mostra all'utente che la raccolta dati è ancora in corso e potrebbe volerci ancora un pò
                MainLabel.Text = "Analizzando la rete"
                MainLabel.Image = My.Resources.bluedot
                SubLabel.Text = "Potrebbe richiedere qualche minuto (" & Convert.ToInt32((DataVar.PingList.Count * 100) / Settings.MinimumPingTry).ToString & "%)"
                Wait()

            End If

            'Controllo delle varie risposti di PingReply
            If dato.Status = IPStatus.Success Then

                'Se non è ancora stato fatto, segnala che il primo ping è andato a buon fine
                firstPingSuccedded = True

            ElseIf dato.Status = IPStatus.TimedOut Then 'Timeout

                'Controlla se il prog. si è mai connesso in questa sessione
                If firstPingSuccedded = False Then
                    For index As Integer = 1 To 10

                        'Invia ping
                        Dim firstDato = SendPing()

                        'Controlla se il primo ping è stato inviato, prova per 5 volte
                        If firstDato.Status = IPStatus.Success Then
                            firstPingSuccedded = True
                            Continue While
                        End If

                        'Aggiorna info
                        SubLabel.Text = $"Tentativo di connessione: {index}/10"

                        'Aspetta qualche secondo
                        Wait()

                    Next

                    'Segnala all'utente che la rete non è disponibile
                    MainLabel.Image = My.Resources.reddot
                    MainLabel.Text = "Impossibile accedere ad internet"
                    SubLabel.Text = "La misurazione riprenderà in automatico quando la rete sarà presente"
                    Wait()

                    'Attendi internet
                    While My.Computer.Network.Ping(Settings.Hostname) = False
                        Wait()
                    End While

                    'Internet funziona
                    firstPingSuccedded = True 'Modificando a true l'utente vedrà questo messaggio
                    WorkerStaticData.RunWorkerAsync()

                End If

            ElseIf dato.Status = IPStatus.HardwareError Then 'Problema con la scheda di rete

                'Modifica controlli
                MainLabel.Text = "Errore della scheda di rete"
                MainLabel.Image = My.Resources.reddot
                SubLabel.Text = "Prova a cambiare driver di rete o controllare il firewall"

                'Esci
                Exit Sub

            End If
        End While

    End Sub

    Sub Wait()
        Application.DoEvents()
        Threading.Thread.Sleep(50)
    End Sub

    Function SendPing() As PingReply

        'Dichiarazione oggetti
        Dim pingClient As Ping = New Ping
        Dim pingSettings As PingOptions = New PingOptions

        'Impostazioni
        pingSettings.DontFragment = True

        'Creo un buffer da inviare
        Dim stringBuffer As String = Settings.Buffer '4 byte di dati
        Dim byteBuffer() As Byte = Encoding.ASCII.GetBytes(stringBuffer)

        'Invio ping
        Return pingClient.Send(Settings.Hostname, DataVar.PingRange, byteBuffer, pingSettings)


    End Function

    Private Sub Home_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        End
    End Sub

    Private Sub WorkerStaticData_DoWork(sender As Object, e As DoWorkEventArgs) Handles WorkerStaticData.DoWork

        'Ottieni dati invariabili dal server
        Dim s As New WebClient
        If My.Computer.Network.Ping("8.8.4.4") Then
            DataVar.Statistics = "Server: " & Settings.Hostname & vbNewLine &
                        "Client: " & s.DownloadString(New Uri("https://ipwhois.app/line/?objects=ip")) & vbNewLine &
                        "ISP: " & s.DownloadString(New Uri("https://ipwhois.app/line/?objects=isp")) & vbNewLine &
                        "Host: " & s.DownloadString(New Uri("https://ipwhois.app/line/?objects=org")) & vbNewLine &
                        "Geo: " & s.DownloadString(New Uri("https://ipwhois.app/line/?objects=city")) & ", " & s.DownloadString(New Uri("https://ipwhois.app/line/?objects=region")) & vbNewLine &
                        "Tipo: " & s.DownloadString(New Uri("https://ipwhois.app/line/?objects=type")) & vbNewLine
        Else
            DataVar.Statistics = "Client: N/A" & vbNewLine &
                             "ISP: N/A" & vbNewLine &
                             "Host: N/A" & vbNewLine &
                             "Geo: N/A" & vbNewLine &
                             "Tipo: N/A" & vbNewLine
        End If

    End Sub
End Class

Public Class Settings

    Public Const Hostname As String = "8.8.4.4"
    Public Const MinimumPingTry As Integer = 500
    Public Const MaxPingTry As Integer = 15000
    Public Const Buffer As String = "aaaa"
    Public Const MaximumJitterMultipler As Double = 3

End Class

Public Class DataVar

    Public Shared Statistics As String = ""
    Public Shared PingList As New List(Of PingReply)
    Public Shared PingRange As Integer = 1000

End Class


