﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Home
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla mediante l'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim ChartArea1 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend1 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Dim Series1 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Home))
        Me.SubLabel = New System.Windows.Forms.Label()
        Me.StartButton = New System.Windows.Forms.Button()
        Me.StatsLabel = New System.Windows.Forms.Label()
        Me.WorkerStaticData = New System.ComponentModel.BackgroundWorker()
        Me.PingChart = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.LoadingPanel = New System.Windows.Forms.PictureBox()
        Me.MainLabel = New System.Windows.Forms.Button()
        CType(Me.PingChart, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LoadingPanel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SubLabel
        '
        Me.SubLabel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SubLabel.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.SubLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(67, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.SubLabel.Location = New System.Drawing.Point(8, 75)
        Me.SubLabel.Name = "SubLabel"
        Me.SubLabel.Size = New System.Drawing.Size(726, 40)
        Me.SubLabel.TabIndex = 1
        Me.SubLabel.Text = "NetAnalyzer di Giacomo Berti"
        Me.SubLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'StartButton
        '
        Me.StartButton.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.StartButton.FlatAppearance.BorderSize = 0
        Me.StartButton.ForeColor = System.Drawing.Color.FromArgb(CType(CType(32, Byte), Integer), CType(CType(33, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.StartButton.Location = New System.Drawing.Point(240, 331)
        Me.StartButton.Name = "StartButton"
        Me.StartButton.Size = New System.Drawing.Size(267, 42)
        Me.StartButton.TabIndex = 2
        Me.StartButton.Text = "Avvia analisi"
        Me.StartButton.UseVisualStyleBackColor = True
        '
        'StatsLabel
        '
        Me.StatsLabel.Font = New System.Drawing.Font("Segoe UI", 11.0!)
        Me.StatsLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(32, Byte), Integer), CType(CType(33, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.StatsLabel.Location = New System.Drawing.Point(12, 138)
        Me.StatsLabel.Name = "StatsLabel"
        Me.StatsLabel.Size = New System.Drawing.Size(225, 169)
        Me.StatsLabel.TabIndex = 3
        Me.StatsLabel.Text = "Server: 8.8.4.4" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Tuo ip: 192.168.1.1" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Ping: 34ms" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Jitter: 22ms" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Uptime: 34%" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Inte" &
    "rnet SP: Wind Tre" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Speed DW: 67mb" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Speed UP: 55mb"
        Me.StatsLabel.Visible = False
        '
        'WorkerStaticData
        '
        '
        'PingChart
        '
        ChartArea1.AlignmentOrientation = CType((System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations.Vertical Or System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations.Horizontal), System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations)
        ChartArea1.AxisX.IntervalOffsetType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number
        ChartArea1.AxisX.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number
        ChartArea1.AxisX.LineWidth = 2
        ChartArea1.AxisX.MajorGrid.Enabled = False
        ChartArea1.AxisX.MajorTickMark.Enabled = False
        ChartArea1.AxisX.Minimum = 0R
        ChartArea1.AxisX2.MajorGrid.Enabled = False
        ChartArea1.AxisX2.MajorTickMark.Enabled = False
        ChartArea1.AxisX2.Minimum = 0R
        ChartArea1.AxisY.LineWidth = 2
        ChartArea1.AxisY.MajorGrid.Enabled = False
        ChartArea1.AxisY.MajorTickMark.Enabled = False
        ChartArea1.AxisY.Minimum = 0R
        ChartArea1.AxisY2.MajorGrid.Enabled = False
        ChartArea1.AxisY2.MajorTickMark.Enabled = False
        ChartArea1.AxisY2.Minimum = 0R
        ChartArea1.BorderColor = System.Drawing.Color.Transparent
        ChartArea1.Name = "ChartArea"
        Me.PingChart.ChartAreas.Add(ChartArea1)
        Legend1.Enabled = False
        Legend1.ForeColor = System.Drawing.Color.White
        Legend1.Name = "Legend1"
        Me.PingChart.Legends.Add(Legend1)
        Me.PingChart.Location = New System.Drawing.Point(210, 129)
        Me.PingChart.Name = "PingChart"
        Me.PingChart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None
        Me.PingChart.PaletteCustomColors = New System.Drawing.Color() {System.Drawing.Color.SeaGreen, System.Drawing.Color.DarkOrange, System.Drawing.Color.Red}
        Series1.BorderColor = System.Drawing.Color.Transparent
        Series1.ChartArea = "ChartArea"
        Series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line
        Series1.Color = System.Drawing.Color.DeepSkyBlue
        Series1.LabelBorderWidth = 0
        Series1.LabelForeColor = System.Drawing.Color.White
        Series1.Legend = "Legend1"
        Series1.MarkerBorderColor = System.Drawing.Color.White
        Series1.MarkerColor = System.Drawing.Color.White
        Series1.Name = "Ping"
        Me.PingChart.Series.Add(Series1)
        Me.PingChart.Size = New System.Drawing.Size(536, 187)
        Me.PingChart.TabIndex = 5
        Me.PingChart.Text = "Grafico ping"
        '
        'LoadingPanel
        '
        Me.LoadingPanel.Cursor = System.Windows.Forms.Cursors.AppStarting
        Me.LoadingPanel.Image = Global.NetAnalyzer.My.Resources.Resources.icons8_delivery_time_96__1_
        Me.LoadingPanel.Location = New System.Drawing.Point(12, 129)
        Me.LoadingPanel.Name = "LoadingPanel"
        Me.LoadingPanel.Size = New System.Drawing.Size(722, 187)
        Me.LoadingPanel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.LoadingPanel.TabIndex = 6
        Me.LoadingPanel.TabStop = False
        Me.LoadingPanel.Visible = False
        '
        'MainLabel
        '
        Me.MainLabel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MainLabel.FlatAppearance.BorderSize = 0
        Me.MainLabel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.MainLabel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.MainLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.MainLabel.Font = New System.Drawing.Font("Segoe UI", 18.0!)
        Me.MainLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(37, Byte), Integer))
        Me.MainLabel.Image = Global.NetAnalyzer.My.Resources.Resources.bluedot
        Me.MainLabel.Location = New System.Drawing.Point(12, 35)
        Me.MainLabel.Name = "MainLabel"
        Me.MainLabel.Size = New System.Drawing.Size(722, 41)
        Me.MainLabel.TabIndex = 0
        Me.MainLabel.Text = "Pronto per l'analisi della rete"
        Me.MainLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.MainLabel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.MainLabel.UseVisualStyleBackColor = True
        Me.MainLabel.UseWaitCursor = True
        '
        'Home
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(749, 403)
        Me.Controls.Add(Me.LoadingPanel)
        Me.Controls.Add(Me.StatsLabel)
        Me.Controls.Add(Me.StartButton)
        Me.Controls.Add(Me.SubLabel)
        Me.Controls.Add(Me.MainLabel)
        Me.Controls.Add(Me.PingChart)
        Me.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.HelpButton = True
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximumSize = New System.Drawing.Size(765, 442)
        Me.MinimumSize = New System.Drawing.Size(765, 442)
        Me.Name = "Home"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "NetAnalyzer"
        CType(Me.PingChart, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LoadingPanel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents MainLabel As Button
    Friend WithEvents SubLabel As Label
    Friend WithEvents StartButton As Button
    Friend WithEvents StatsLabel As Label
    Friend WithEvents WorkerStaticData As System.ComponentModel.BackgroundWorker
    Friend WithEvents PingChart As DataVisualization.Charting.Chart
    Friend WithEvents LoadingPanel As PictureBox
End Class
