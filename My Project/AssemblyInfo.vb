﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' Le informazioni generali relative a un assembly sono controllate dal seguente 
' set di attributi. Modificare i valori di questi attributi per modificare le informazioni
' associate a un assembly.

' Controllare i valori degli attributi degli assembly

<Assembly: AssemblyTitle("NetAnalyzer")>
<Assembly: AssemblyDescription("Misuratore di rete scritto in .NET")>
<Assembly: AssemblyCompany("Giacomo Berti")>
<Assembly: AssemblyProduct("NetAnalyzer")>
<Assembly: AssemblyCopyright("Giacomo Bert")>
<Assembly: AssemblyTrademark("Giacomo Berti")>

<Assembly: ComVisible(False)>

'Se il progetto viene esposto a COM, il GUID seguente verrà usato come ID del typelib
<Assembly: Guid("b89dd4cd-d344-4b66-9047-550792571ecf")>

' Le informazioni sulla versione di un assembly sono costituite dai seguenti quattro valori:
'
'      Versione principale
'      Versione secondaria
'      Numero di build
'      Revisione
'
' È possibile specificare tutti i valori oppure impostare valori predefiniti per i numeri relativi alla revisione e alla build
' usando l'asterisco '*' come illustrato di seguito:
' <Assembly: AssemblyVersion("1.0.*")>

<Assembly: AssemblyVersion("1.0.0.0")>
<Assembly: AssemblyFileVersion("1.0.0.0")>
